# ttXPheno code

The idea behind this code is to use RIVET as if it were a complete analysis chain, rather than simply a way to compare MC generators at particle-level. This code was designed to be used on studies for ttX (X=W,Z,H,gamma) but it should be obvious how to adapt it to whatever you need. It relies on some ROOT libraries and outputs an NTuple with three trees:
  - `Parton level`: Mostly this relies on hacky things with the MC history and was specifically NOT what RIVET was designed to do.
  - `Particle level`: Objects (Projections) built from stable objects in the MC history, useful for safe MC-MC comparisons.
  - `Reco level`: Particle level objects that have been smeared to approximate reco-level objects for ATLAS and/or CMS.
  
It should work with RIVET version 2.6.1 onwards, but the reco level functions will only work correctly with 2.6.2 onwards. It should work out-of-the-box on CERN lxplus7 CentOS machines.

The code was written by James Howarth.

## Setting up RIVET at CERN

Log into a CERN lxplus7 machine:

        ssh -XY username@lxplus7.cern.ch
        
Navigate to a directory where you want to run (somewhere with a lot of space is a good idea as HEPMC files are very large) and then install the latest version of RIVET following the instructions on https://rivet.hepforge.org/trac/wiki/GettingStarted. Once this has finished and installed (it should do so without any problems).

Once this has completed you should source this file:
        
        local/rivetenv.sh
        
Now you should be good to go!

### Compiling the code

Checkout a version of this repository after setting up RIVET and then compile using the following:

        rivet-buildplugin --with-root RivetPHENO_ttX.so PHENO_ttX.cc
        
You can then run the code using:

        rivet -a PHENO_ttX --analysis-path ./ InputFiles/ttbar_hepmc
        
which will produce a rather unimaginatively named file called "output_file.root". You might want to change this particular behaviour if you plan to run multiple jobs in the same directory to avoid overwriting previous results.
